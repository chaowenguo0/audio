import asyncio, aiohttp, bs4, builtins, re, lxml.etree, argparse, pathlib, sys, tempfile, shutil

parser = argparse.ArgumentParser()
parser.add_argument('title')
args = parser.parse_args()
text = pathlib.Path(__file__).resolve().parent.joinpath(args.title + '.txt').read_text().split('\n')
speak = lxml.etree.Element('speak', nsmap={None:'http://www.w3.org/2001/10/synthesis', 'mstts':'http://www.w3.org/2001/mstts'}, attrib={'{http://www.w3.org/XML/1998/namespace}lang':'zh-HK', 'version':'1.0'})
voice = lxml.etree.SubElement(speak, 'voice', attrib={'name':'zh-HK-HiuGaaiNeural'})
#lxml.etree.SubElement(voice, '{http://www.w3.org/2001/mstts}silence', attrib={'type':'Leading-exact', 'value':'0'})
#lxml.etree.SubElement(voice, '{http://www.w3.org/2001/mstts}silence', attrib={'type':'Tailing-exact', 'value':'0'})
mp4 = pathlib.Path(__file__).resolve().parent.joinpath(args.title + '.mp4')
mp4.unlink(True)

async def main(text):
    voice.text = text.replace('\u3000', '')
    async with aiohttp.ClientSession() as session:
        async with session.get('https://www.bing.com/translator', params={'from':'yue'}) as translator:
            html = bs4.BeautifulSoup(await translator.text(), 'lxml')
            params_RichTranslateHelper = builtins.next(_ for _ in builtins.str(html.find('script', string=re.compile('var params_RichTranslateHelper =')).string).split(';') if _.startswith(' var params_RichTranslateHelper =')).split('[')[1].split(',')
            async with session.post(f'https://www.bing.com/tfetspktok', params={'isVertical':1, 'IG':builtins.next(_ for _ in builtins.str(html.find('script', string=re.compile('IG:')).string).split(',') if _.startswith('IG:')).split('"')[1], 'IID':html.find('div', attrs={'data-iid':True}).get('data-iid')}, data={'token':params_RichTranslateHelper[1].split('"')[1], 'key':params_RichTranslateHelper[0]}, headers={'user-agent':'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36'}) as tfetspktok:
                _ = await tfetspktok.json()
                token, region = _.get('token'), _.get('region')
                async with session.post(f'https://{region}.tts.speech.microsoft.com/cognitiveservices/v1', headers={'authorization':f'Bearer {token}', 'content-type':'application/ssml+xml', 'x-microsoft-outputformat':'audio-48khz-192kbitrate-mono-mp3'}, data=lxml.etree.tostring(speak, encoding='UTF-8')) as response:
                    print(response.status)
                    with tempfile.NamedTemporaryFile() as tmp:
                        ffmpeg = await asyncio.create_subprocess_exec('ffmpeg', '-y', '-vaapi_device', '/dev/dri/renderD128', '-hwaccel', 'vaapi', '-hwaccel_output_format', 'vaapi', '-loop', '1', '-i', args.title + '.jpg', '-i', '-', '-c:v', 'h264_vaapi', '-c:a', 'copy', '-vf', f'hwdownload,format=nv12,pad=iw:ih+40,drawtext=fontfile=package/usr/share/fonts/opentype/noto/NotoSerifCJK-Bold.ttc:text={builtins.chr(10).join(voice.text[_:_ + 30] for _ in range(0, builtins.len(voice.text), 30))}:y=h-th:fontcolor=white:fontsize=10,hwupload', '-shortest', '-f', 'mp4', tmp.name, stdin=asyncio.subprocess.PIPE)
                        await ffmpeg.communicate(await response.content.read())
                        if not mp4.exists(): shutil.copy(tmp.name, mp4)
                        else:
                            concat = await asyncio.create_subprocess_exec('ffmpeg', '-hwaccel', 'vaapi', '-hwaccel_output_format', 'vaapi', '-f', 'concat', '-safe', '0', '-protocol_whitelist', 'file,pipe', '-i', 'pipe:0', '-c', 'copy', '-f', 'mp4', '-movflags', 'frag_keyframe+empty_moov', 'pipe:1', stdin=asyncio.subprocess.PIPE, stdout=asyncio.subprocess.PIPE)
                            mp4.write_bytes((await concat.communicate(f'''file {pathlib.Path(__file__).resolve().parent.joinpath(args.title + '.mp4')}
file {tmp.name}'''.encode()))[0])

for _ in text: asyncio.run(sys.modules[__name__].main(_))
#https://github.com/soimort/translate-shell/blob/develop/include/Translators/BingTranslator.awk
#iconv -f gb18030 -t utf8 1.txt -o 2.txt 把gb18030编码的1.txt转换成utf8的2.txt
#awk -i inplace -v RS=\\r\\n 1 txt
#unzip -p docx word/document.xml | sed -e 's/<[^>]\{1,\}>//g; s/[^[:print:]]\{1,\}//g'
#apt install -y --no-install-recommends poppler-utils#pdftotext pdf txt#awk -i inplace {gsub\(/\f/\,\"\"\,\$0\)}1 txt