import {promises as fs} from 'fs'
import {auth} from 'google-auth-library'
import process from 'process'

const client = auth.fromJSON(globalThis.JSON.parse(await fs.readFile('gcloud', 'utf8')))
client.scopes = ['https://www.googleapis.com/auth/cloud-platform']

const text = globalThis.Object.freeze([...await fs.readFile(process.argv.at(2) + '.txt', 'utf8')])
const interval = 1000

async function main(begin)
{
    const response = await client.request({url:'https://texttospeech.googleapis.com/v1/text:synthesize', method:'post', body:globalThis.JSON.stringify({input:{text:text.slice(begin * interval, (begin + 1) * interval).join('')}, voice:{languageCode:'yue-hk', name:'yue-HK-Standard-A', ssmlGender:'female'}, audioConfig:{audioEncoding:'mp3'}})})
    const json = response.data
    console.log(response.status, json)
    if (!globalThis.Object.is(response.status, 200)) throw true
    await fs.appendFile(process.argv.at(2) + '.mp3', json.audioContent, 'base64')
}

for (const _ of globalThis.Array(globalThis.Math.ceil(text.length / interval)).keys())
{
    await main(_)
    await new globalThis.Promise(_ => globalThis.setTimeout(_, 1000 * 5))
}
//https://cloud.google.com/text-to-speech/docs/quickstart-protocol
//https://cloud.google.com/text-to-speech/docs/basics
//单次限制2500000
//wc -m file//get number of utf-8 characters