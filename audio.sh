#qsub -F file -l nodes=1:gen11:ppn=2,walltime=24:00:00 audio.sh
rm -rf audio.sh.*
python3 audio.py $1
python3 storage.py mp3 $1.mp4
max=$(ffprobe -v error -select_streams v:0 -show_entries stream=duration -of default=noprint_wrappers=1:nokey=1 -sexagesimal $1.mp4 | awk -F: {print\$1})
for i in $(seq 0 $(($max / 11)))
do
    ffmpeg -ss $((11 * $i)):00:00 -t 11:00:00 -i $1.mp4 -c copy $1$i.mp4
done