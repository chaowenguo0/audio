public class Main
{
    private static final int interval = 2000, increase = 50;
    private static final java.net.http.HttpResponse.BodyHandler<java.lang.String> bodyHandler = java.net.http.HttpResponse.BodyHandlers.ofString();
    private static final java.net.http.HttpClient client = java.net.http.HttpClient.newBuilder().build();
    private static final com.fasterxml.jackson.databind.ObjectMapper objectMapper = new com.fasterxml.jackson.databind.ObjectMapper();
    private static java.lang.String title;
    private static java.util.List<java.lang.String> text;
    private static void write(final int begin, final java.lang.String key) throws java.lang.Exception
    {
	final var doc = javax.xml.parsers.DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
	final var speak = doc.createElement("speak");
	speak.setAttribute("version", "1.0");
        speak.setAttribute("xml:lang", "en-us");
        final var voice = doc.createElement("voice");
        voice.setAttribute("name", "zh-HK-HiuGaaiNeural");
	voice.appendChild(doc.createTextNode(java.lang.String.join("", text.subList(begin * interval, (begin + 1) * interval))));
	speak.appendChild(voice);
	doc.appendChild(speak);
	final var transformer = javax.xml.transform.TransformerFactory.newInstance().newTransformer();
	final var writer = new java.io.StringWriter();
        transformer.transform(new javax.xml.transform.dom.DOMSource(doc), new javax.xml.transform.stream.StreamResult(writer));
	final var response = client.sendAsync(java.net.http.HttpRequest.newBuilder(java.net.URI.create("https://westus2.tts.speech.microsoft.com/cognitiveservices/v1")).headers("authorization", "Bearer " + client.sendAsync(java.net.http.HttpRequest.newBuilder(java.net.URI.create("https://westus2.api.cognitive.microsoft.com/sts/v1.0/issueToken")).headers("ocp-apim-subscription-key", key, "content-length", "0").POST(java.net.http.HttpRequest.BodyPublishers.noBody()).build(), bodyHandler).thenApply(java.net.http.HttpResponse::body).join(), "content-type", "application/ssml+xml", "x-microsoft-outputformat", "audio-24khz-48kbitrate-mono-mp3").POST(java.net.http.HttpRequest.BodyPublishers.ofString(writer.toString())).build(), java.net.http.HttpResponse.BodyHandlers.ofByteArray()).join();
	java.lang.System.out.println(response.statusCode());
	if (response.statusCode() != 200) throw new java.lang.Exception(new java.lang.String(response.body()));
	java.nio.file.Files.write(java.nio.file.Paths.get(title + ".mp3"), response.body(), java.nio.file.StandardOpenOption.APPEND, java.nio.file.StandardOpenOption.CREATE);
    }
    
    public static void batch(final int begin) throws java.lang.Exception
    {
        final var token = objectMapper.readValue(client.sendAsync(java.net.http.HttpRequest.newBuilder(java.net.URI.create("https://login.microsoftonline.com/deb7ba76-72fc-4c07-833f-1628b5e92168/oauth2/token")).POST(java.net.http.HttpRequest.BodyPublishers.ofString("grant_type=client_credentials&client_id=60f0699c-a6da-4a59-be81-fd413d2c68bc&client_secret=ljEw3qnk.HcDcd85aSBLgjdJ4uA~bqPKYz&resource=https://management.azure.com/")).build(), bodyHandler).thenApply(java.net.http.HttpResponse::body).join(), java.util.Map.class).get("access_token");
        final var subscription = "326ccd13-f7e0-4fbf-be40-22e42ef93ad5";
        final var group = java.lang.String.format("https://management.azure.com/subscriptions/%s/resourcegroups/speech?api-version=2021-04-01", subscription);
        if (client.sendAsync(java.net.http.HttpRequest.newBuilder(java.net.URI.create(group)).header("authorization", "Bearer " + token).method("HEAD", java.net.http.HttpRequest.BodyPublishers.noBody()).build(), bodyHandler).thenApply(java.net.http.HttpResponse::statusCode).join() == 204)
        {
            final var response = client.sendAsync(java.net.http.HttpRequest.newBuilder(java.net.URI.create(group)).header("authorization", "Bearer " + token).DELETE().build(), bodyHandler).join();
            if (response.statusCode() == 202)
            {
                final var header = response.headers().map();
                while (true)
                {
                    java.util.concurrent.TimeUnit.SECONDS.sleep(java.lang.Long.parseLong(header.get("retry-after").get(0)));
                    if (client.sendAsync(java.net.http.HttpRequest.newBuilder(java.net.URI.create(header.get("location").get(0))).headers("authorization", "Bearer " + token).build(), bodyHandler).thenApply(java.net.http.HttpResponse::statusCode).join() == 200) break;
                }
            }
        }
        client.sendAsync(java.net.http.HttpRequest.newBuilder(java.net.URI.create(group)).headers("authorization", "Bearer " + token, "content-type", "application/json").PUT(java.net.http.HttpRequest.BodyPublishers.ofString(objectMapper.writeValueAsString(java.util.Map.entry("location", "westus2")))).build(), bodyHandler).join();
        final var random = new java.security.SecureRandom();
	final var name = java.lang.String.valueOf(random.nextLong());
        client.sendAsync(java.net.http.HttpRequest.newBuilder(java.net.URI.create(java.lang.String.format("https://management.azure.com/subscriptions/%s/resourceGroups/speech/providers/Microsoft.CognitiveServices/accounts/%s?api-version=2021-10-01", subscription, name))).headers("authorization", "Bearer " + token, "content-type", "application/json").PUT(java.net.http.HttpRequest.BodyPublishers.ofString(objectMapper.writeValueAsString(java.util.Map.ofEntries(java.util.Map.entry("location", "westus2"), java.util.Map.entry("kind", "SpeechServices"), java.util.Map.entry("sku", java.util.Map.entry("name", "F0")))))).build(), bodyHandler).join();
        final var response = client.sendAsync(java.net.http.HttpRequest.newBuilder(java.net.URI.create(java.lang.String.format("https://management.azure.com/subscriptions/%s/resourceGroups/speech/providers/Microsoft.CognitiveServices/accounts/%s/listKeys?api-version=2021-10-01", subscription, name))).headers("authorization", "Bearer " + token, "content-length", "0").POST(java.net.http.HttpRequest.BodyPublishers.noBody()).build(), bodyHandler).join();
        final var key = java.lang.String.valueOf(objectMapper.readValue(response.body(), java.util.Map.class).get("key1"));
	java.util.concurrent.TimeUnit.SECONDS.sleep(10);
	for (final var $:(java.lang.Iterable<java.lang.Integer>)java.util.stream.IntStream.range(begin, java.lang.Math.min(begin + increase, (int)java.lang.Math.ceil(text.size() / interval)))::iterator)
	{
	    write($, key);
            java.util.concurrent.TimeUnit.SECONDS.sleep(2);
	}
    }
	
    public static void main(final java.lang.String[] args) throws java.lang.Exception
    {
	title = args[0];
	text = java.nio.file.Files.readString(java.nio.file.Paths.get(args[0] + ".txt")).codePoints().mapToObj(java.lang.Character::toString).collect(java.util.stream.Collectors.toList());
        //for _ in range(0, len(text) // (interval * increase)): await batch(_ * increase)
        for (final var $:(java.lang.Iterable<java.lang.Integer>) java.util.stream.IntStream.range(0,  (int)java.lang.Math.ceil(text.size() / (interval * increase)))::iterator) batch($ * increase);
    }
}
